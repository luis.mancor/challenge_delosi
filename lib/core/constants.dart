import 'package:flutter/services.dart';

class Constants {
  static const platform = MethodChannel("com.luismancor.kotlin.snackbar");

  static const String appName = 'Matrix App';
  static const String errorMessageInvalidFormat = 'Formato de matriz inválido.';
  static const String errorMessageInvalidListContent =
      'La lista debe contener valores enteros.';
  static const String errorMessageNotAList = 'Ingrese una lista válida.';
  static const String errorMessageNonNumericElement =
      'La matriz contiene elementos no numéricos.';
  static const String errorMessageNotSquareMatrix = 'La matriz no es cuadrada.';
}
