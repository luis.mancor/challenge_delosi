abstract class AppException implements Exception {}

class InvalidMatrixFormatException extends AppException {
  final String message;
  InvalidMatrixFormatException(this.message);

  @override
  String toString() => message;
}

class InvalidListException extends AppException {
  final String message;
  InvalidListException(this.message);

  @override
  String toString() => message;
}

class InvalidContentListException extends AppException {
  final String message;
  InvalidContentListException(this.message);

  @override
  String toString() => message;
}

class NotSquareMatrixException extends AppException {
  final String message;
  NotSquareMatrixException(this.message);

  @override
  String toString() => message;
}
