import 'package:challenge_csti/features/matrix/presentation/blocs/matrix_bloc.dart';
import 'package:challenge_csti/features/matrix/presentation/screens/matrix_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

final GoRouter router = GoRouter(
  routes: <GoRoute>[
    GoRoute(
      path: '/',
      builder: (BuildContext context, GoRouterState state) {
        return BlocProvider(
          create: (context) => MatrixBloc(),
          child: MatrixPage(),
        );
      },
      routes: <GoRoute>[
        GoRoute(
          path: 'matrix',
          builder: (BuildContext context, GoRouterState state) {
            return const Placeholder();
          },
        ),
      ],
    ),
  ],
);
