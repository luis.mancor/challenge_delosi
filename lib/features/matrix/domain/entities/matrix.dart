import 'package:equatable/equatable.dart';

class Matrix extends Equatable {
  final List<List<int>> matrix;

  const Matrix({required this.matrix});

  @override
  List<Object?> get props => [matrix];
}
