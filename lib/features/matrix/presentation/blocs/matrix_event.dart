part of 'matrix_bloc.dart';

abstract class MatrixEvent extends Equatable {
  const MatrixEvent();

  @override
  List<Object> get props => [];
}

class InitialMatrixEvent extends MatrixEvent {}

class EdditingMatrixEvent extends MatrixEvent {
   final String matrix;
    
  const EdditingMatrixEvent({required this.matrix});

  @override
  List<Object> get props => [matrix];
}


class LoadingMatrixEvent extends MatrixEvent {
   final String matrix;

  const LoadingMatrixEvent({required this.matrix});

  @override
  List<Object> get props => [matrix];
}

class ErrorMatrixEvent extends MatrixEvent {
  final String message;

  const ErrorMatrixEvent({required this.message});

  @override
  List<Object> get props => [message];
}
