part of 'matrix_bloc.dart';

abstract class MatrixState extends Equatable {
  const MatrixState();

  @override
  List<Object?> get props => [];
}

class MatrixInitialState extends MatrixState {}

class MatrixEdditingState extends MatrixState {
  final List<List<int>> matrix;

  const MatrixEdditingState({required this.matrix});
  @override
  List<Object?> get props => [matrix];
}

class MatrixLoadingState extends MatrixState {}

class MatrixLoadedState extends MatrixState {
  final List<List<int>> matrix;
  final List<List<int>> previousMatrix;
  const MatrixLoadedState({required this.matrix, required this.previousMatrix});

  @override
  List<Object?> get props => [matrix];
}

class MatrixErrorState extends MatrixState {
  final String message;

  const MatrixErrorState({required this.message});

  @override
  List<Object?> get props => [message];
}
