import 'dart:convert';

import 'package:challenge_csti/core/constants.dart';
import 'package:challenge_csti/features/matrix/presentation/blocs/matrix_bloc.dart';
import 'package:challenge_csti/features/matrix/presentation/widgets/matrix_display_error_widget.dart';
import 'package:challenge_csti/features/matrix/presentation/widgets/matrix_display_widget.dart';
import 'package:challenge_csti/features/matrix/presentation/widgets/matrix_initial_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MatrixPage extends StatelessWidget {
  MatrixPage({super.key});
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Juego de matrices"),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _controller,
                    decoration: const InputDecoration(
                      isDense: true,
                      labelText: "Matriz",
                      border: OutlineInputBorder(),
                      hintText: "Ingrese una matriz",
                    ),
                    onChanged: (value) {
                      context
                          .read<MatrixBloc>()
                          .add(EdditingMatrixEvent(matrix: _controller.text));
                    },
                  ),
                ),
                const SizedBox(width: 10),
                ElevatedButton(
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      context
                          .read<MatrixBloc>()
                          .add(LoadingMatrixEvent(matrix: _controller.text));
                    },
                    child: const Text("Rotar")),
              ],
            ),
          ),
          const SizedBox(height: 14),
          Expanded(
            child: BlocListener<MatrixBloc, MatrixState>(
              listener: (context, state) async {
                if (state is MatrixLoadedState) {
                  _controller.text = jsonEncode(state.matrix);
                  await Constants.platform.invokeMethod('showToast', {
                    'message':
                        "La matriz anterior fue:\n${_matrixToString(state.previousMatrix)}"
                  });
                } else if (state is MatrixErrorState) {
                  await Constants.platform
                      .invokeMethod('showToast', {'message': state.message});
                }
              },
              child: BlocBuilder<MatrixBloc, MatrixState>(
                builder: (context, state) {
                  if (state is MatrixLoadingState) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is MatrixEdditingState) {
                    return MatrixDisplayWidget(matrix: state.matrix);
                  } else if (state is MatrixLoadedState) {
                    return MatrixDisplayWidget(matrix: state.matrix);
                  } else if (state is MatrixErrorState) {
                    return MatrixDisplayErrorWidget(message: state.message);
                  }
                  return const MatrixInitialDisplayWidget();
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  String _matrixToString(List<List<int>>? matrix) {
    if (matrix == null) return "";
    return matrix.map((row) => row.join(' ')).join('\n');
  }
}
