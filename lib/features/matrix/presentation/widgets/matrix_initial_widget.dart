import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class MatrixInitialDisplayWidget extends StatelessWidget {
  const MatrixInitialDisplayWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            height: MediaQuery.of(context).size.height / 2,
            child: Stack(
              children: [
                const RiveAnimation.asset('assets/sarah_cat.riv',
                    animations: ['Tail Animation', 'Eye Blink 01'],
                    fit: BoxFit.cover),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text("Ingrese valores de matriz",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headlineSmall),
                )
              ],
            )));
  }
}
