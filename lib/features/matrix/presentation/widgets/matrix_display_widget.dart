import 'package:flutter/material.dart';

class MatrixDisplayWidget extends StatelessWidget {
  const MatrixDisplayWidget({super.key, required this.matrix});

  final List<List<int>> matrix;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: GridView.builder(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: matrix.isNotEmpty ? matrix[0].length : 1,
          mainAxisSpacing: 10.0,
          crossAxisSpacing: 10.0,
        ),
        itemCount: matrix.isNotEmpty ? matrix.length * matrix[0].length : 1,
        itemBuilder: (context, index) {
          int row = index ~/ matrix[0].length;
          int colum = index % matrix[0].length;
          return Container(
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.primary.withOpacity(0.5),
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Center(
                child: Text(
              matrix[row][colum].toString(),
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(color: Theme.of(context).colorScheme.onPrimary),
            )),
          );
        },
      ),
    );
  }
}

