import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class MatrixDisplayErrorWidget extends StatelessWidget {
  final String message;
  const MatrixDisplayErrorWidget({
    super.key,
    required this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
            height: MediaQuery.of(context).size.height / 2,
            child: Stack(
              children: [
                const RiveAnimation.asset('assets/noresultscat.riv',
                    fit: BoxFit.cover),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(message,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headlineSmall),
                )
              ],
            )));
  }
}
