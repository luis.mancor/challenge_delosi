import 'dart:async';
import 'dart:developer';

import 'package:challenge_csti/core/app/app_bloc_observer.dart';
import 'package:challenge_csti/core/router/app_router.dart';
import 'package:challenge_csti/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    Bloc.observer = const AppBlocObserver();
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    runApp(const MyApp());
    FlutterError.onError = (details) {
      log(details.exceptionAsString(), stackTrace: details.stack);
      FirebaseCrashlytics.instance.recordFlutterFatalError(details);
    };
  }, (error, stack) {
    //# Todo log app errors globally
    log(error.toString(), stackTrace: stack);
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: 'Matrix App Challenge',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueGrey),
        useMaterial3: true,
      ),
      routerConfig: router,
    );
  }
}
