import UIKit
import Flutter
import Toast_Swift

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
      let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
      let toastChannel = FlutterMethodChannel(name: "com.luismancor.kotlin.snackbar",
                                                 binaryMessenger: controller.binaryMessenger)
      toastChannel.setMethodCallHandler({
         [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
           guard call.method == "showToast" else {
               result(FlutterMethodNotImplemented)
               return
             }
             let arguments = call.arguments as! [String: Any]
             let msg = arguments["message"] as! String
             self?.showToast(message: msg)
           result(nil)
      })
    
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func showToast(message: String){
        DispatchQueue.main.async {
            var style = ToastStyle()
            style.messageColor = .white
            style.backgroundColor = .gray
            self.window?.rootViewController?.view.makeToast(message, duration: 4.0, position: .bottom, style: style)
        }
    }
}
